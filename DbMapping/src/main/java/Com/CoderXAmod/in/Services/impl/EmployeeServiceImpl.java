package Com.CoderXAmod.in.Services.impl;

import Com.CoderXAmod.in.Entities.Employee;
import Com.CoderXAmod.in.Services.EmployeeService;
import Com.CoderXAmod.in.repository.EmployeeRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

@Service
public class EmployeeServiceImpl implements EmployeeService {
    @Autowired
    private EmployeeRepository employeeRepository;
    @Override
    public Employee Add_Employee(Employee employee) {
        return employeeRepository.save(employee);
    }
}
