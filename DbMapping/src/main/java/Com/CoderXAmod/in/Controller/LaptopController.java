package Com.CoderXAmod.in.Controller;

import Com.CoderXAmod.in.Entities.Laptop;
import Com.CoderXAmod.in.Services.LaptopService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RestController;

@RestController
public class LaptopController {
    @Autowired
    LaptopService laptopService;
    @PostMapping("/CreateLaptop")
    public ResponseEntity<Laptop> creteLaptop(@RequestBody Laptop laptop)
    {
        Laptop createdLaptop = laptopService.CreateLaptop(laptop);
        return new ResponseEntity<>(createdLaptop, HttpStatus.CREATED);
    }
}
