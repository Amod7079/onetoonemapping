package Com.CoderXAmod.in.repository;

import Com.CoderXAmod.in.Entities.Laptop;
import org.springframework.data.jpa.repository.JpaRepository;

public interface LaptopRepository extends JpaRepository<Laptop,Integer> {
}
