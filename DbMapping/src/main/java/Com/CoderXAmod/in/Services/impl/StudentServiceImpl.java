package Com.CoderXAmod.in.Services.impl;

import Com.CoderXAmod.in.Entities.Student;
import Com.CoderXAmod.in.Services.StudentService;
import Com.CoderXAmod.in.repository.StudentRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.UUID;

@Service
public class StudentServiceImpl implements StudentService {
    @Autowired
    private StudentRepository studentRepository;

    @Override
    public Student CreateStudent(Student student) {
        String string = UUID.randomUUID().toString();
        student.setId(string);
        return studentRepository.save(student);
    }

    @Override
    public void DeleteStudentFromServer() {
    }

    @Override
    public List<Student> getAllStudent() {
        return studentRepository.findAll();
    }
}
