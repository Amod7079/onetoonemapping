package Com.CoderXAmod.in.Services;

import Com.CoderXAmod.in.Entities.Laptop;
import Com.CoderXAmod.in.Entities.Student;

import java.util.List;

public interface LaptopService {
    Laptop CreateLaptop(Laptop laptop);
    void DeleteLaptopFromServer();
    List<Laptop> getAllLaptop();
}
