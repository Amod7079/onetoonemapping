package Com.CoderXAmod.in.Controller;

import Com.CoderXAmod.in.Entities.Student;
import Com.CoderXAmod.in.Services.StudentService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RestController;

@RestController
public class StudentController {
    @Autowired
    private StudentService studentService;

    @PostMapping("/CreateStudent")
    public ResponseEntity<Student> CreateStudent(@RequestBody Student student) {
        Student studentd = studentService.CreateStudent(student);
        return new ResponseEntity<>(studentd, HttpStatus.CREATED);
    }
}
