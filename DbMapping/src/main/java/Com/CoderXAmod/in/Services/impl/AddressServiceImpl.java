package Com.CoderXAmod.in.Services.impl;

import Com.CoderXAmod.in.Entities.Address;
import Com.CoderXAmod.in.Services.AddressService;
import Com.CoderXAmod.in.repository.AddressRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

@Service
public class AddressServiceImpl implements AddressService {

    @Autowired
    private AddressRepository addressRepository;
    @Override
    public Address Add_Address(Address address) {
        return addressRepository.save(address);
    }
}
