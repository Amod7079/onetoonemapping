package Com.CoderXAmod.in.repository;

import Com.CoderXAmod.in.Entities.Employee;
import org.springframework.data.jpa.repository.JpaRepository;

public interface EmployeeRepository extends JpaRepository<Employee,Integer> {
}
