package Com.CoderXAmod.in.Services.impl;

import Com.CoderXAmod.in.Entities.Laptop;
import Com.CoderXAmod.in.Entities.Student;
import Com.CoderXAmod.in.Services.LaptopService;
import Com.CoderXAmod.in.repository.LaptopRepository;
import Com.CoderXAmod.in.repository.StudentRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public class LaptopServiceImpl implements LaptopService {

    @Autowired
    private LaptopRepository laptopRepository;

    @Autowired
    private StudentRepository studentRepository;

    @Override
    public Laptop CreateLaptop(Laptop laptop) {
        return laptopRepository.save(laptop);
    }

    @Override
    public void DeleteLaptopFromServer() {

    }

    @Override
    public List<Laptop> getAllLaptop() {
        return null;
    }
}
