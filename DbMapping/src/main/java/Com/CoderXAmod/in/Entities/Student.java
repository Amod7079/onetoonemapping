package Com.CoderXAmod.in.Entities;

import jakarta.persistence.Entity;
import jakarta.persistence.Id;
import jakarta.persistence.OneToOne;
import lombok.*;
import org.springframework.beans.factory.annotation.Value;

@Getter
@Setter
@NoArgsConstructor
@AllArgsConstructor
@Builder
@ToString
@Entity
public class Student {
    @Id
    private String id;
    private String name="JhonDoe";
    private String about;
    @OneToOne(mappedBy = "student")
    private Laptop laptop;
}
