package Com.CoderXAmod.in.repository;

import Com.CoderXAmod.in.Entities.Student;
import org.springframework.data.jpa.repository.JpaRepository;

public interface StudentRepository extends JpaRepository<Student,String> {
}
