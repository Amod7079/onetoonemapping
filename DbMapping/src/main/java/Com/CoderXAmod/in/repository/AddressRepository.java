package Com.CoderXAmod.in.repository;

import Com.CoderXAmod.in.Entities.Address;
import org.springframework.data.jpa.repository.JpaRepository;

public interface AddressRepository extends JpaRepository<Address,Integer> {
}
