package Com.CoderXAmod.in.Services;

import Com.CoderXAmod.in.Entities.Student;
import java.util.List;

public interface StudentService {
     Student CreateStudent(Student student);
     void DeleteStudentFromServer();
     List<Student> getAllStudent();
}
