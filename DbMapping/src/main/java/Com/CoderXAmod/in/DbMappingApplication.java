package Com.CoderXAmod.in;

import Com.CoderXAmod.in.Entities.Student;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class DbMappingApplication {

	public static void main(String[] args) {
		SpringApplication.run(DbMappingApplication.class, args);


	}

}
