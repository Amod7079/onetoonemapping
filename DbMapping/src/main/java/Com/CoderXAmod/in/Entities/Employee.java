package Com.CoderXAmod.in.Entities;

import jakarta.persistence.Entity;
import jakarta.persistence.Id;
import jakarta.persistence.OneToOne;
import lombok.*;

@Getter
@Setter
@NoArgsConstructor
@AllArgsConstructor
@Builder
@ToString
@Entity
public class Employee {

    @Id
    private int id;
    private String  name;
    @OneToOne
    private Address address;
}
