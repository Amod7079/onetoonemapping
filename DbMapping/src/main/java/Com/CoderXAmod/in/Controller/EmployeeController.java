package Com.CoderXAmod.in.Controller;

import Com.CoderXAmod.in.Entities.Employee;
import Com.CoderXAmod.in.Services.EmployeeService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
public class EmployeeController {
    @Autowired
    private EmployeeService employeeService;
    @PostMapping("/createEmployee")
    public ResponseEntity<Employee> Add_Employee(Employee employee)
    {
        Employee newEmployee = employeeService.Add_Employee(employee);
        return new ResponseEntity<>(newEmployee, HttpStatus.CREATED);
    }
}
