package Com.CoderXAmod.in.Controller;

import Com.CoderXAmod.in.Entities.Address;
import Com.CoderXAmod.in.Services.AddressService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RestController;

@RestController
public class AddressController {
    @Autowired
    private AddressService addressService;

    @PostMapping("/createAddress")
    public ResponseEntity<Address> Add_Address(@RequestBody Address address) {
        Address createdAddress = addressService.Add_Address(address);
        return new ResponseEntity<>(createdAddress, HttpStatus.CREATED);
    }
}
